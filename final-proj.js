(function() {
  const API_BASE = 'https://api.openweathermap.org/data/2.5/forecast';
  const MONTHS = [
    'January', 
    'February', 
    'March', 
    'April', 
    'May', 
    'June', 
    'July', 
    'August', 
    'September', 
    'October', 
    'November', 
    'December'
  ];

  const getForecastDate = function() {
    let forecastDate = new Date();
    // if after noon, forecast for noon the next day
    if (forecastDate.getHours() > 12) {
      forecastDate.setDate(forecastDate.getDate() + 1);
    }
    forecastDate.setHours(12);
    forecastDate.setMinutes(0);
    forecastDate.setSeconds(0);
    forecastDate.setMilliseconds(0);

    return forecastDate;
  }

  const getOutfit = function(temp) {
    return outfits.filter(function(outfit) {
      return outfit.degrees < temp;
    })[0];
  }

  const degreeEl = document.getElementById('degrees');
  const dateEl = document.getElementById('date');
  const cityEl = document.getElementById('city');
  const outfitDescEl = document.getElementById('outfit-descr');
  const outfitImgEl = document.getElementById('outfit-img');
  const forecastInfo = document.getElementById('forecast-info');

  HTMLElement.prototype.fade = function(shouldShow = true, delay = 0) {
    let that = this;
    setTimeout(function() {
      if (shouldShow) {
        that.classList.remove('hidden');
      } else {
        that.classList.add('hidden');
      }
    }, delay)
  }

  const showNewEls = function() {
    degreeEl.fade();
    forecastInfo.fade(true, 200);
    outfitDescEl.fade(true, 400);
    outfitImgEl.fade(true, 600);
  }

  const hideCurrentEls = function () {
    degreeEl.fade(false);
    forecastInfo.fade(false);
    outfitDescEl.fade(false);
    outfitImgEl.fade(false);
  }

  const updateHtml = function(temp, city, forecastDate) {
    const day = forecastDate.getDate() === (new Date()).getDate() ?
        'today' :
        'tomorrow';
    const monthName = MONTHS[forecastDate.getMonth()];

    degreeEl.innerHTML = `${temp.toFixed(1)}&deg;`;
    dateEl.innerHTML = `${day}, <b>${monthName} ${forecastDate.getDate()}</b>`;
    cityEl.innerText = city;
    
    const outfit = getOutfit(temp);
    outfitDescEl.innerText = outfit.description;
    outfitImgEl.src = outfit.imageUrl;
    
    showNewEls();
  }

  const errorEl = document.getElementById('error');
  const getWeather = function(zip) {
    const forecastDate = getForecastDate();
    const forecastDateInSeconds = forecastDate.getTime() / 1000;

    const url = `${API_BASE}?zip=${zip}&units=imperial&appid=${API_KEY}`;
    fetch(url)
      .then(function(response) {
        if (!response.ok) {
          errorEl.fade(true);
        } else {
          errorEl.fade(false);
        }
        return response.json();
      })
      .then(function(forecastJson) {
        // find only forecast items after forecastDate
        let forecastItem = forecastJson.list.filter(function(listItem) {
          return listItem.dt > forecastDateInSeconds;
        });

        const city = (forecastJson.city.name);
        const temp = (forecastItem[0].main.temp);
        updateHtml(temp, city, forecastDate);
        document.querySelector('.outfit').classList.remove('d-none');
      });
  }

  const zipForm = document.getElementById('zip-form');
  const zipInput = document.getElementById('zip');
  const zipButton = document.getElementById('zip-btn');

  zipInput.addEventListener('input', function() {
    if(zipInput.validity.patternMismatch) {
      zipInput.setCustomValidity("Expects 5 digit postal code");
    } else {
      zipInput.setCustomValidity("");
    }
  })

  zipInput.addEventListener('focus', function() {
    zipButton.style.display = 'inline';
  });

  zipForm.addEventListener('submit', function(e) {
    e.preventDefault();
    zipButton.style.display = 'none';
    
    hideCurrentEls();
    getWeather(zipInput.value);

    // Set cookie w/ expiration to one month
    document.cookie = `weatherZip=${zipInput.value};max-age=2592000`;
  });

  const zipCookie = /weatherZip=([^;]+)/.exec(document.cookie);
  if (zipCookie && zipCookie.length === 2) {
    const lastZipCode = zipCookie[1];
    getWeather(lastZipCode);
    document.getElementById('zip').value = lastZipCode;
  }
})();