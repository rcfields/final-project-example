# Final Project

## Setup

1. Get Open Weather Map API Key [here](https://openweathermap.org/)

2. Create apikey.js file with the following code:

```javascript
// Replace {{your_api_key}} with api key
const API_KEY = "{{your_api_key}}";
```